Calculator logic explained:

**Assumed the input is valid.

- expression is a string
- get every char of the expression and check its type (operator / number)
- if it's number 
	- check if the previous char is also a number (!isPreviousOperator)
		- if not, add it to a values array (that will work as a stack)
		- if it is, then combine the chars to obtain the full number (and update the value in stack)
- if it's operator 
	- check if the previous operator has a higher priority
		- if not, add it to the operators array (that will work as a stack)
		- if so, then we should calculate the subtotal & after that reset operators stack and add the new operator

- subtotal calculate algorithm
	- based on maxPriority, the algorithm pops the first value and operator with max priority
	- while the values stack is not empty, pop values and calculate them

- for the history I've added a calculationHistory array in the service so we'll also have state management at service level
	- the new value is added on the first position of the array and not pushed at the end of the array