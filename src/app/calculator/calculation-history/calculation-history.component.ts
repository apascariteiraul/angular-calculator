import { Component } from '@angular/core';
import { CalculatorService } from '../calculator.service';

@Component({
  selector: 'calculation-history',
  templateUrl: './calculation-history.component.html',
  styleUrls: ['./calculation-history.component.scss']
})
export class CalculationHistoryComponent {

  constructor(private calculatorService: CalculatorService) { 
    
  }

  get calculationsHistory() {
    return this.calculatorService.calculationHistory;
  }

}
