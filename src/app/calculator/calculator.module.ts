import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input'

import { CalculatorRoutingModule } from './calculator-routing.module';
import { CalculatorComponent } from './calculator/calculator.component';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CalculationHistoryComponent } from './calculation-history/calculation-history.component';
import { MatFormFieldModule } from '@angular/material/form-field';


@NgModule({
  declarations: [
    CalculatorComponent,
    CalculationHistoryComponent
  ],
  imports: [
    CommonModule,
    CalculatorRoutingModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    FlexLayoutModule
  ]
})
export class CalculatorModule { }
