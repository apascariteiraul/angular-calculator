export interface Operator {
  operator: OperatorsEnum,
  priority: number
}

export enum OperatorsEnum {
  'add' = '+',
  'sub' = '-',
  'mul' = '*',
  'div' = '/'
}

export const OPERATORS_PRIORITY: Operator[] = [
  {
    operator: OperatorsEnum.add,
    priority: 0
  },
  {
    operator: OperatorsEnum.sub,
    priority: 0
  },
  {
    operator: OperatorsEnum.mul,
    priority: 1
  },
  {
    operator: OperatorsEnum.div,
    priority: 1
  }
]