import { Injectable } from '@angular/core';
import { Operator, OperatorsEnum, OPERATORS_PRIORITY } from './models/Operator';

@Injectable({
  providedIn: 'root'
})
export class CalculatorService {  
  private _calculationHistory: string[] = [];

  constructor() { }

  public get calculationHistory() {
    return this._calculationHistory;
  }
  
  public set calculationHistory(v) {
    this._calculationHistory = v;
  }

  calculateExpression(expression: string): string {
    // values & operators will be used as stacks
    var values: string[] = [];
    var operators: Operator[] = [];
    var isPreviousOperator = true;

    // iterate through each character of expression
    for(var i=0; i < expression.length; i++) {
      // check if the selected character is an operator
      if (Object.values(OperatorsEnum).includes(<any>expression[i])) {
        // get the operator priority info
        const operator = OPERATORS_PRIORITY.find(op => op.operator === expression[i]);
        
        // if the last operator added has a lower or equal priority compared to the current one, push it to the operators stacks
        if (operator && ((operators.length && operators[operators.length-1].priority <= operator.priority) || !operators.length)) {
          operators.push(operator);
        } else { // if the current operator has a lower priority, calculate the current subtotal & update the stacks

          // reset operators stack & store the calculated value in new values stack
          const subtotal = this.calculateSubtotal(values, operators)
          if (subtotal) {
            values = [subtotal];  
          }
          if (operator) {
            operators = [operator];
          }
        }

        // set flag for the following char
        isPreviousOperator = true;
      } else {
        // if the character is not an operator, check if the previous character was operator
        if (isPreviousOperator) {
          // if true, then push the new number into array
          values.push(expression[i]);
        } else {
          // if false, that means that both the previous and the current characters are numbers (ex: 52)
          // so I'll update 5 to 52 in array
          values[values.length-1] = values[values.length-1].concat(expression[i]);
        }

        // set flag for the following char
        isPreviousOperator = false;
      }
    }

    const result = this.calculateSubtotal(values, operators);
    this.addCalculInHistory(expression + " = " + result);
    return result;
  }

  calculateSubtotal(values: string[], operators: Operator[]) {
    // based on maxPriority, the algorithm pops the first value and operator with max priority
    let maxPriority = this.getMaxPriority(operators);
    let tempVal1 = this.popNextValue(operators, values, maxPriority);
    while (values.length) {
      // get all values and operators one by one - based on the previous comment explanation
      // the temporary calculated value will be stored in tempVal1
      maxPriority = this.getMaxPriority(operators);
      const tempVal2 = this.popNextValue(operators, values, maxPriority);
      const tempOp = this.popNextOperator(operators, maxPriority);

      if (tempVal1 && tempVal2) {
        switch (tempOp?.operator) {
          case OperatorsEnum.add:
            tempVal1 = (parseFloat(tempVal1) + parseFloat(tempVal2)).toString();
            break;
          case OperatorsEnum.sub:
            tempVal1 = (parseFloat(tempVal1) - parseFloat(tempVal2)).toString();
            break;
          case OperatorsEnum.mul:
            tempVal1 = (parseFloat(tempVal1) * parseFloat(tempVal2)).toString();
            break;
          case OperatorsEnum.div:
            tempVal1 = (parseFloat(tempVal1) / parseFloat(tempVal2)).toString();
            break;
        }
      }
    }
    return tempVal1;
  }

  popNextValue(operators: Operator[], values: string[], maxPriority: number) {
    const maxPriorityIndex = operators.findIndex(op => op.priority == maxPriority);
    const nextValue = values[maxPriorityIndex];
    values.splice(maxPriorityIndex, 1);
    return nextValue;
  }

  popNextOperator(operators: Operator[], maxPriority: number) {
    const maxPriorityIndex = operators.findIndex(op => op.priority == maxPriority);
    const nextOperator = operators[maxPriorityIndex];
    operators.splice(maxPriorityIndex, 1);
    return nextOperator;
  }

  getMaxPriority(operators: Operator[]) {
    return Math.max(...operators.map(o => o.priority));
  }

  addCalculInHistory(calcul: string) {
    this._calculationHistory.splice(0, 0, calcul);
  }
}
