import { Component } from '@angular/core';
import { CalculatorService } from '../calculator.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent {
  result!: string;

  constructor(private calculatorService: CalculatorService) { 
    
  }

  calculate(expression: string) {
    this.result = this.calculatorService.calculateExpression(expression);
  }

}
